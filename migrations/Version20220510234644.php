<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220510234644 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE categorie (id_cat INT AUTO_INCREMENT NOT NULL, nom_cat VARCHAR(30) NOT NULL, PRIMARY KEY(id_cat)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE produits ADD id_cat INT DEFAULT NULL, ADD stock INT NOT NULL, ADD icone VARCHAR(255) NOT NULL, ADD stars INT DEFAULT NULL, DROP disponible, CHANGE description description VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE produits ADD CONSTRAINT FK_475BBDDAFAABF2 FOREIGN KEY (id_cat) REFERENCES categorie (id_cat)');
        $this->addSql('CREATE INDEX IDX_475BBDDAFAABF2 ON produits (id_cat)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE Produits DROP FOREIGN KEY FK_475BBDDAFAABF2');
        $this->addSql('DROP TABLE categorie');
        $this->addSql('DROP INDEX IDX_475BBDDAFAABF2 ON Produits');
        $this->addSql('ALTER TABLE Produits ADD disponible TINYINT(1) NOT NULL, DROP id_cat, DROP stock, DROP icone, DROP stars, CHANGE description description LONGTEXT NOT NULL');
    }
}
