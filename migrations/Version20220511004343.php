<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220511004343 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE produit (id_pdt INT AUTO_INCREMENT NOT NULL, id_cat INT DEFAULT NULL, nom_pdt VARCHAR(255) NOT NULL, prix DOUBLE PRECISION NOT NULL, description VARCHAR(30) NOT NULL, stock INT NOT NULL, icone VARCHAR(255) NOT NULL, stars INT DEFAULT NULL, INDEX IDX_29A5EC27FAABF2 (id_cat), PRIMARY KEY(id_pdt)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE produit ADD CONSTRAINT FK_29A5EC27FAABF2 FOREIGN KEY (id_cat) REFERENCES categorie (id_cat)');
        $this->addSql('ALTER TABLE produits DROP FOREIGN KEY FK_475BBDDAFAABF2');
        $this->addSql('DROP INDEX IDX_475BBDDAFAABF2 ON produits');
        $this->addSql('ALTER TABLE produits DROP id_cat, DROP stock, DROP icone, DROP stars, CHANGE description description LONGTEXT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE produit');
        $this->addSql('ALTER TABLE Produits ADD id_cat INT DEFAULT NULL, ADD stock INT NOT NULL, ADD icone VARCHAR(255) NOT NULL, ADD stars INT DEFAULT NULL, CHANGE description description VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE Produits ADD CONSTRAINT FK_475BBDDAFAABF2 FOREIGN KEY (id_cat) REFERENCES categorie (id_cat)');
        $this->addSql('CREATE INDEX IDX_475BBDDAFAABF2 ON Produits (id_cat)');
    }
}
