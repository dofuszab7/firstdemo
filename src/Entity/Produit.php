<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use App\Repository\ProduitRepository;


/**
 * Produit
 *
 * @ORM\Table(name="produit", indexes={@ORM\Index(name="id_cat", columns={"id_cat"})})
* @ORM\Entity(repositoryClass=ProduitRepository::class)
 */
class Produit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_pdt", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPdt;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_pdt", type="string", length=30, nullable=false)
     */
    private $nomPdt;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float", precision=10, scale=0, nullable=false)
     */
    private $prix;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="stock", type="integer", nullable=false)
     */
    private $stock;

    /**
     * @var string
     *
     * @ORM\Column(name="icone", type="string", length=255, nullable=false)
     * @Assert\File(mimeTypes={"image/jpeg"})
     */
    private $icone;
    /**
     * @ORM\Column(type="array")
     */
    private $produit;
     /**
     * @ORM\Column(type="boolean")
     */
    private $disponible;
    /*
    * @ORM\ManyToOne(targetEntity=Commandes::class, inversedBy="produits") 
    * @ORM\JoinColumn(nullable=true)
    */
   private $commande;
   /**
     * * @var int
     *
     * @ORM\Column(name="stars", type="integer", nullable=true)
     */
    private $stars;

    /**
     * @var \Categorie
     *
     * @ORM\ManyToOne(targetEntity="Categorie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cat", referencedColumnName="id_cat")
     * })
     */
    private $idCat;

    public function getIdPdt(): ?int
    {
        return $this->idPdt;
    }

    public function getNomPdt(): ?string
    {
        return $this->nomPdt;
    }

    public function setNomPdt(string $nomPdt): self
    {
        $this->nomPdt = $nomPdt;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }
    public function getStars(): ?int
    {
        return $this->stars;
    }

    public function setStars(int $stars): self
    {
        $this->stars = $stars;

        return $this;
    }
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setStock(int $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    public function getIcone()
    {
        return $this->icone;
    }

    public function setIcone($icone)
    {
        $this->icone = $icone;

        return $this;
    }

    public function getIdCat(): ?Categorie
    {
        return $this->idCat;
    }

    public function setIdCat(?Categorie $idCat): self
    {
        $this->idCat = $idCat;

        return $this;
    }

    public function getDisponible(): ?bool
    {
        return $this->disponible;
    }

    public function setDisponible(bool $disponible): self
    {
        $this->disponible = $disponible;

        return $this;
    }
    public function getProduit()
    {
        return $this->produit;
    }

    public function setProduit($produit): self
    {
        $this->produit = $produit;

        return $this;
    }
}
