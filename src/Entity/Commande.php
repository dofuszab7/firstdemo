<?php

namespace App\Entity;

use App\Repository\CommandeRepository;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CommandeRepository::class)
 */
class Commande
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @var Livraison|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Livraison", inversedBy="commandes", cascade={"persist"})
     * @ORM\JoinColumn(name="livraison_id", referencedColumnName="id")
     * @Assert\NotBlank(message="La commande est obligatoire.")

     */
    private $livraison;


    public function __toString()
    {
        return (string) $this->getId();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param mixed $libelle
     * @return Commande
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
        return $this;
    }

    /**
     * @return Livraison|null
     */
    public function getLivraison(): ?Livraison
    {
        return $this->livraison;
    }

    /**
     * @param Livraison|null $livraison
     * @return Commande
     */
    public function setLivraison(?Livraison $livraison): Commande
    {
        $this->livraison = $livraison;
        return $this;
    }
}
