<?php

namespace App\Entity;

use App\Repository\LivraisonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=LivraisonRepository::class)
 */
class Livraison
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank(message="Le titre doit être renseigner")
     * @Assert\Length(
     *      min = 5,
     *      minMessage="Le titre doit contenir minimum 5 caractère"
     *
     *     )
     * @ORM\Column(type="string", length=255)
     */
    private $nom;


   /**
     * @Assert\Positive
     * @ORM\Column(type="integer", length=255)
    * @Assert\NotBlank(message="Le prix doit être renseigner")
     */
    private $prix;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le type de livraison doit être renseigner")
     *
     */
    private $typelivraison;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Commande", mappedBy="livraison", cascade={"persist", "remove"})
     * @Assert\Count(
     *      min = "1",
     *      minMessage = "Vous devez renseignér minimum une commande",
     * )
     */
    private $commandes;

    public function __construct()
    {
        $this->commandes = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    /**
     * @return mixed
     */
    public function getTypelivraison()
    {
        return $this->typelivraison;
    }

    /**
     * @param mixed $typelivraison
     */
    public function setTypelivraison($typelivraison): void
    {
        $this->typelivraison = $typelivraison;
    }


    public function setPrix(?float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }


    public function getCommandes()
    {
        return $this->commandes;
    }

    /**
     * @param mixed $commande
     */
    public function addCommande($commande)
    {
        $this->commandes->add($commande);
        $commande->setLivraison($this);
    }

    /**
     * @param mixed $commande
     */
    public function removeCommande($commande)
    {
        $this->commandes->removeElement($commande);
        // uncomment if you want to update other side
        //$commande->setLivraison(null);
    }
}
