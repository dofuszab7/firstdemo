<?php

namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use App\Repository\CommandesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table("commandes")
 * @ORM\Entity(repositoryClass=CommandesRepository::class)
 */
class Commandes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    

    /**
     * @ORM\Column(type="boolean")
     */
    private $valider;

    /**
    
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="integer")
     */
    private $reference;
 /**
     * @ORM\OneToMany(targetEntity=Produits::class, mappedBy="commande")
     *@ORM\JoinColumn(nullable=true)
     */
    private $produits;
    /**
     * @ORM\Column(type="array")
     */
    private $commande;

     /**
     
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="commandes") 
     * @ORM\JoinColumn(nullable=true)
     */
    private $utilisateur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValider(): ?bool
    {
        return $this->valider;
    }

    public function setValider(bool $valider): self
    {
        $this->valider = $valider;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getReference(): ?int
    {
        return $this->reference;
    }

    public function setReference(int $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getCommande()
    {
        return $this->commande;
    }

    public function setCommande($commande): self
    {
        $this->commande = $commande;

        return $this;
    }
}
