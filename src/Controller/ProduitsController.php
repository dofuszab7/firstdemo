<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ProduitsRepository;
use App\Entity\Produits;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

use App\Repository\CommandesRepository;
use App\Entity\Commandes;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProduitsController extends AbstractController
{
    

//     /**
//      * @Route("/produit", name="produits")
//      */
//     public function produitAction(): Response
//     {
//         $produits=$this->getDoctrine()->getManager()->getRepository(Produits::class)->findAll();

//      return $this->render('produits/produits.html.twig',[
//     'pr'=>$produits
//     ]);
// }

    /**
     * @Route("/presentationproduit{id}", name="presentation")
     */
    public function presentationAction($id,Request $request,SessionInterface $session){
        $session=$request->getSession(); 
        
        $produit=$this->getDoctrine()->getManager()->getRepository(Produits::class)->find($id);
        if(!$produit) throw $this->createNotFoundException('La page n\'existe pas.');
        if (!$session->has('panier')) 
        $panier=$session->set('panier',array());
               else{
                   $panier=false;
        return $this->render('produits/presentation.html.twig',[
            'produit'=>$produit,'panier'=>$panier
            ]);



    }}

//     /**
//      * @Route("/produit", name="produits")
//      */
//     public function produitAction(Request $request,SessionInterface $session): Response
//     {
       
//          $session=$request->getSession(); 
//          $produits=$this->getDoctrine()->getManager()->getRepository(Produits::class)->findBy(array('disponible'=>1));
//          if(!$session->has('panier'))
//          // $session->set('panier',array());
//          $panier=$session->get('panier');
         
//                 else{
//                     $panier=false;
                    
            
              
//     }
//     return $this->render('produits/produits.html.twig',[
//         'pr'=>$produits,'panier'=>$panier
//         ]);
// }
}