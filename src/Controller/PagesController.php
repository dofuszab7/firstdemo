<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Component\HttpFoundation\Session\Session;


class PagesController extends AbstractController
{
 


    /**
     * @Route("/menu", name="menu")
     */
    public function menuAction(): Response
    {
        $em=$this->getDoctrine()->getManager();
        $pages=$em->getRepository('PagesRepository')->findAll();
        return $this->render('pages/menu.html.twig', [
            'pages' => $pages,
        ]);
    }

    /**
     * @Route("/page{id}", name="page")
     */
    public function pageAction($id): Response
    {
       
        return $this->render('pages/pages.html.twig');
        
    }
    
     /**
     * @Route("/home", name="home")
     */
    public function home(FlashyNotifier $flashy)
    {
       return $this->render('pages/home.html.twig');
        
    }
    

}
