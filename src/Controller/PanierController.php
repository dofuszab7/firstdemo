<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Request;

use App\Repository\ProduitRepository;
use App\Entity\Produit;
use App\Repository\CommandesRepository;
use App\Entity\Commandes;
use App\Repository\UserRepository;
use App\Entity\User;
use App\Form\UserType;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Security\Core\Security;
use Dompdf\Dompdf;
use Dompdf\Options;




class PanierController extends AbstractController
{

    /**
     * @Route("/menupanier", name="menupanier")
     */
    public function menuAction(Request $request,SessionInterface $session): Response
    {
        $session=$request->getSession();
        if (!$session->has('panier'))
        $articles=0;
        else
        $articles=count($session->get('panier'));
        return $this->render('panier/article.html.twig',['articles'=>$articles]);
        

    }
     /**
     * @Route("/ajouter{id}", name="ajouter")
     */
    public function ajouterAction($id,Request $request,SessionInterface $session): Response
    {
       
         $session=$request->getSession(); 
         if(!$session->has('panier')) $session->set('panier',array());
               $panier=$session->get('panier');
         if(array_key_exists($id,$panier))
               {
                    if ($request->query->get('qte')!= null) $panier[$id]=$request->query->get('qte');
                    $this->addFlash('success','Quantité modifié avec succès');
                }
                else{
              if ($request->query->get('qte')!= null) 
               $panier[$id] = $request->query->get('qte');
               
             else 
                    $panier[$id]=1;
                    $this->addFlash('success','Article ajouté avec succès');
               }
               $session->set('panier',$panier);
              

      
       
       return $this->redirectToRoute('panier');     
    }
    
    


    /**
     * @Route("/action", name="panier")
     */
    public function panierAction(Request $request,SessionInterface $session): Response
    {
        $session=$request->getSession();
         if(!$session->has('panier')) $session->set('panier',[]);
         $produits=$this->getDoctrine()->getManager()->getRepository(Produit::class)->findArray(array_keys($session->get('panier')));
         
         //var_dump($session->get('panier'));
         
         return $this->render('panier/panier.html.twig',[
            'produits'=>$produits,'panier'=>$session->get('panier')]);
    }

     /**
     * @Route("/validation", name="validation")
     */
    public function validationAction(Request $request,SessionInterface $session): Response
    {
      
        //if ($request->getMethod() == 'POST');
        //$this->setLivraisonOnSession($request,$session);
        
        $session=$request->getSession();
        $em=$this->getDoctrine()->getManager();
       
        if (!$session->has('commande'))
            $commande =new Commandes();
            

        
       else  
        $commande=$em->getRepository(Commandes::class)->find($session->get('commande'));
        
        
       
        $commande->setDate(new \DateTime);
        //$commande=$this->container->get('security.context')->getToken()->getUser();
      // $commande->setUtilisateur($this->container->get('security.context')->getToken()->getUser());
        $commande->setValider(0);
        $commande->setReference(0);
        
        
        if (!$session->has('commande')){
        $em->persist($commande);
        $session->set('commande',$commande);
        }
    
         $em->flush();
        //----------------
        

        $em=$this->getDoctrine()->getManager();
        $preparecommande=$this->forward('App\Controller\CommandesController::preparecommande');
        
        $commande=$em->getRepository(Commandes::class)->find($preparecommande->getContent());
       //$session=$request->getSession();  
        //$reference=$session->get('nom');

        //$adresse=$session->get('adressse');

        $produits=$em->getRepository(Produit::class)->findArray(array_keys($session->get('panier')));
        //$utilisateur=$em->getRepository(Utilisateur::class)->find($adresse['utilisateur']);
        //$facturation=$em->getRepository(Utilisateur::class)->find($adresse['facturation']);
            
     
     //return $this->render('panier/validation.html.twig',[
       
        //'produits'=>$produits,'utilisateur'=>$utilisateur,'facturation'=>$facturation,'panier'=>$session->get('panier')]);
        $this->addFlash('success','Votre panier est validé');
        
       return $this->render('panier/validation.html.twig',[
       'produits'=>$produits,'commande'=>$commande,'panier'=>$session->get('panier')]);
       die();
      //return $this->render('panier/validation.html.twig',[
      // 'commande'=>$commande]);

       
    }
   
    /**
     * @Route("/supprimer{id}", name="supprimer_panier")
     */
    public function supprimerAction($id,Request $request){
        $session=$request->getSession();
        $panier=$session->get('panier');

        if(array_key_exists($id,$panier))
        {
            unset($panier[$id]);
            $session->set('panier',$panier);
            $this->addFlash('success','Article supprimé avec succès');

        }
        return $this->redirectToRoute('panier');  
    }
    

    //  /**
    //   * @Route("/livraison", name="livraison")
    //   */

    //  public function livraisonAction(Request $request,Security $security){
 
    //     // $utilisateur=$this->container->get('security.context')->getToken()->getUser();          
    //     $entity=new Utilisateur();
        
    //   $utilisateur = $security->getUser();      
    
   
    //     //$form=$this->createForm(UtilisateurType::class,$entity);
    //     $form=$this->createForm(UtilisateurType::class,$entity);
        
    //       if($request->getMethod() =='POST')
    //    {
    //         $form->handleRequest($request);
    //         if($form->isValid){
                
    //             $em=$this->getDoctrine()->getManager();
    //             $entity->setUtilisateur($utilisateur);
                
               
                
    //             $em->persist($entity);
    //             $em->flush();
               
    //             return $this->redirectToRoute('livraison');}}
    //             return $this->render('panier/livraison.html.twig',['utilisateur'=>$utilisateur,'form'=>$form->createView()]);
    //             //return $this->render('panier/livraison.html.twig',['form'=>$form->createView()]);
    //         }
       
 
    
// /**
//      * @Route("/setlivraison", name="setlivraison")
//      */

//     public function setLivraisonOnSession(Request $request,SessionInterface $session): Response{
//         $session=$request->getSession();
//         if(!$session->has('adresse')) $session->set('adresse',[]);
//         $adresse=$session->get('adresse');
//         if($request->request->get('livraison') != null && $request->request->get('facturation') != null)
//         {
//             $adresse['livraison']=$request->get('livraison');
//             $adresse['facturation']=$request->get('facturation');
            

//         }
//         else{
//             return $this->redirectToRoute('validation');
//         }
//         $session->set('adresse',$adresse);
//         return $this->redirectToRoute('validation');
        
//     }
        
  /**
     * @Route("/listep", name="commande_list",methods={"GET"})
     */
    public function listep(Request $request,SessionInterface $session): Response
    {
        
         $pdfOptions=new Options();
         $pdfOptions->set('defaultFont','Arial');

         $dompdf=new Dompdf($pdfOptions);
         $em=$this->getDoctrine()->getManager();
         $preparecommande=$this->forward('App\Controller\CommandesController::preparecommande');
         $commande=$em->getRepository(Commandes::class)->find($preparecommande->getContent());
        $session=$request->getSession();  
        $produits=$em->getRepository(Produit::class)->findArray(array_keys($session->get('panier')));
         
         
         $html=$this->renderView('panier/listp.html.twig',[
            'produits'=>$produits,'panier'=>$session->get('panier')]);
        // Load HTML to Dompdf
        $dompdf->loadHtml($html);
        
        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream("mypdf.pdf", [
            "Attachment" => false
        ]);}
        
    

}
