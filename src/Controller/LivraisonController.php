<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Livraison;
use App\Form\LivraisonType;
use App\Repository\LivraisonRepository;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\PieChart;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/livraison")
 */
class LivraisonController extends AbstractController
{
    /**
     * @Route("/", name="app_livraison_index", methods={"GET"})
     */
    public function index(LivraisonRepository $livraisonRepository): Response
    {
        return $this->render('livraison/index.html.twig', [
            'livraisons' => $livraisonRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_livraison_new", methods={"GET", "POST"})
     */
    public function new(Request $request, LivraisonRepository $livraisonRepository): Response
    {
        $livraison = new Livraison();
        $form = $this->createForm(LivraisonType::class, $livraison);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($livraison->getCommandes() as $commande){
                $livraison->addCommande($commande);
            }
            $livraisonRepository->add($livraison);
            return $this->redirectToRoute('app_livraison_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('livraison/create.html.twig', [
            'livraison' => $livraison,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_livraison_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Livraison $livraison, LivraisonRepository $livraisonRepository): Response
    {
        $form = $this->createForm(LivraisonType::class, $livraison);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($livraison->getCommandes() as $commande){
                $livraison->addCommande($commande);
            }
            $livraisonRepository->add($livraison);
            return $this->redirectToRoute('app_livraison_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('livraison/update.html.twig', [
            'livraison' => $livraison,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/suppLivraison/{id}", name="app_livraison_delete")
     */
    public function delete(Livraison $livraison): Response
    {
        $em= $this->getDoctrine()->getManager();
        $em->remove($livraison);
        $em->flush();
        return $this->redirectToRoute('app_livraison_index');
    }


    /**
     * @Route("/l/livraison_stat", name="livraison_stat")
     */
    public function livraison_stat(): Response
    {
        $pieChart = new PieChart();
        $em = $this->getDoctrine();

        $data = array();
        $stat = ['Les livraisons', '%'];
        array_push($data, $stat);

        $e1 = $em->getRepository(Livraison::class)->findBy(array('typelivraison' => "Decoration"));
        $total = count($e1);
        $stat = ["Decoration", $total];
        array_push($data, $stat);
        $stat = array();
        $e2 = $em->getRepository(Livraison::class)->findBy(array('typelivraison' => "vetements"));
        $total = count($e2);
        $stat = ["vetements", $total];
        array_push($data, $stat);
        $stat = array();
        $e3 = $em->getRepository(Livraison::class)->findBy(array('typelivraison' => "accessoires"));
        $total = count($e3);
        $stat = ["accessoires", $total];
        array_push($data, $stat);
        $stat = array();
        $pieChart->getData()->setArrayToDataTable(
            $data
        );

        $pieChart->getOptions()->setTitle('Les Livraisons');
        $pieChart->getOptions()->setHeight(500);
        $pieChart->getOptions()->setWidth(900);
        $pieChart->getOptions()->getTitleTextStyle()->setBold(true);
        $pieChart->getOptions()->getTitleTextStyle()->setColor('#009900');
        $pieChart->getOptions()->getTitleTextStyle()->setItalic(true);
        $pieChart->getOptions()->getTitleTextStyle()->setFontName('Arial');
        $pieChart->getOptions()->getTitleTextStyle()->setFontSize(20);


        return $this->render('livraison/stat.html.twig', [
            'piechart' => $pieChart
        ]);
    }
}
