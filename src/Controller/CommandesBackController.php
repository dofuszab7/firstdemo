<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Request;

use App\Repository\ProduitRepository;
use App\Entity\Produit;
use App\Repository\CommandesRepository;
use App\Entity\Commandes;
use App\Repository\UserRepository;
use App\Entity\User;
use App\Form\UserType;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Security\Core\Security;
use Dompdf\Dompdf;
use Dompdf\Options;

class CommandesBackController extends AbstractController
{
    /**
     * @Route("/validationartisan", name="validationartisan")
     */
    public function validationAction(Request $request,SessionInterface $session): Response
    {
      
        if ($request->getMethod() == 'POST');
        //$this->setLivraisonOnSession($request,$session);
        

        $em=$this->getDoctrine()->getManager();
        $preparecommande=$this->forward('App\Controller\CommandesController::preparecommande');
        $commande=$em->getRepository(Commandes::class)->find($preparecommande->getContent());

        
        $session=$request->getSession();  
        //$reference=$session->get('nom');

        //$adresse=$session->get('adressse');

        $produits=$em->getRepository(Produit::class)->findArray(array_keys($session->get('panier')));
        //$utilisateur=$em->getRepository(Utilisateur::class)->find($adresse['utilisateur']);
        //$facturation=$em->getRepository(Utilisateur::class)->find($adresse['facturation']);
            
     //var_dump($commande);
     //return $this->render('panier/validation.html.twig',[
       
        //'produits'=>$produits,'utilisateur'=>$utilisateur,'facturation'=>$facturation,'panier'=>$session->get('panier')]);
        $this->addFlash('success','Les commandes affichés avec succès');
       return $this->render('artisan/validation.html.twig',[
        'produits'=>$produits,'commande'=>$commande,'panier'=>$session->get('panier')]);
     // return $this->render('panier/validation.html.twig',[
       // 'commande'=>$commande]);
    }
     /**
     * @Route("/commandes/back", name="app_commandes_back")
     */
    public function index(): Response
    {
        return $this->render('back.html.twig');
    }
}
