<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\DependencyInjection\ContainerBuilder;

use App\Repository\CommandesRepository;
use App\Entity\Commandes;
use App\Repository\ProduitsRepository;
use App\Entity\Produits;
use App\Repository\UserRepository;
use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
 //----mobile----//
 use Symfony\Component\Serializer\Encoder\JsonEncoder;
 use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
 use Symfony\Component\Serializer\Serializer;
 use Symfony\Component\Validator\Constraints\Json;
 //-------------//
class CommandesController extends AbstractController
{


 /**
      * @Route("/facture", name="facture")
      */

     public function facture(Request $request,SessionInterface $session): Response

     {
     $em=$this->getDoctrine()->getManager();
     $generator = random_bytes(20); 
     $session=$request->getSession();
     //$adresse=$session->get('adresse');
     $panier=$session->get('panier');
     $commande=[];
     $totalHT=0;
     //$totalTTC=0;
    //$facturation=$em->getRepository(Utilisateur::class)->find($adresse['facturation']);
    //$livraison=$em->getRepository(Utilisateur::class)->find($adresse['livraison']);
    $produits=$em->getRepository(Produits::class)->findArray(array_keys($session->get('panier')));
     
     foreach ($produits as $produit)
     {
         $prixHT=($produit->getPrix() * $panier[$produit->getId()]);
         $totalHT+=$prixHT;
     $commande['produit'][$produit->getId()]=array('reference'=> $produit->getNom(),
                                                    'quantite'=>$panier[$produit->getId()],
                                                    'prixHT'=>round($produit->getPrix(),2));
                                                   
                                                   
                                                    
                                                 }
    
//    $commande['livraison']= array('prenom'=>$livraison->getPrenom(),
//                                       'nom'=>$livraison->getNom(),
//                                         'telephone'=>$livraison->getNom(),
//                                      'adresse'=>$livraison->getAdresse(),
//                                          'cp'=>$livraison->getCp(),
//                                          'ville'=>$livraison->getVille(),
//                                          'pays'=>$livraison->getPays(),
//                                      'complement'=>$livraison->getComplement());

    
//      $commande['facturation']= array('prenom'=>$facturation->getPrenom(),
//                                      'nom'=>$facturation->getNom(),
//                                        'telephone'=>$facturation->getNom(),
//                                     'adresse'=>$facturation->getAdresse(),
//                                         'cp'=>$facturation->getCp(),
//                                         'ville'=>$facturation->getVille(),
//                                         'pays'=>$facturation->getPays(),
//                                     'complement'=>$facturation->getComplement());

                                    
     $commande['prixHT']=round($totalHT,2);
    $commande['token'] = $generator;  
          
                   
  // return $commande;   
   return $this->redirectToRoute('facture');
    
   //return $this->render('commandes/facture.html.twig',[
      //  'commande'=>$commande]);
     // return new JsonResponse($commande);

 }  


/**
     * @Route("/preparecommande", name="preparecommande")
     */

    public function preparecommandeAction(Request $request,SessionInterface $session): Response

    {

        $session=$request->getSession();
        $em=$this->getDoctrine()->getManager();
       
        if (!$session->has('commande'))
            $commande =new Commandes();
            

        
       else  
        $commande=$em->getRepository(Commandes::class)->find($session->get('commande'));
        
       
       
        $commande->setDate(new \DateTime);
        //$commande=$this->container->get('security.context')->getToken()->getUser();
      // $commande->setUtilisateur($this->container->get('security.context')->getToken()->getUser());
        $commande->setValider(0);
        $commande->setReference(0);
        $commande->setCommande($this->facture($request,$session));
        
        if (!$session->has('commande')){
        $em->persist($commande);
        $session->set('commande',$commande);
        }
    
         $em->flush();
        
         return new Response ($commande->getId());
       
   // return $this->render('commandes/preparecommande.html.twig',[
     //   'commande'=>$commande]);

    }


    
 

    /**
       * @Route("/validationCommande{id}", name="validationCommande")
      */
  public function validationCommandeAction($id){
      $em=$this->getDoctrine()->getManager();
      $commande=$em->getRepository(Commandes::class)->find($id); 
      if(!commande || $commande->getValider()==1)
      throw $this->createNotFoundException('La commande n\'existe pas');
      $commande->setValider(1);
      $commande->setReference(1);
      $em->flush();
      $session=$this->getRequest()->getSession();
      $session->remove('adresse');
      $session->remove('panier');
      $session->remove('commande');
      $this->get('session')->addFlash('success','Votre commande est validé avec succès');
      return $this->redirectToRoute('produits');  
    

  }

   /**
     * @Route("/index", name="index")
     */
    public function indexAction($stripeSK,Request $request): Response
    {
        \Stripe\Stripe::setApiKey($stripeSK);
        \Stripe\Charge::create(array(
            "amount"=>1000,
            "currency"=>"eur",
            "source"=>"tok_visa",
            "description"=>"Paiement de test"
        ));
        $this->addFlash('success','Paiement est fait avec succès');
        return $this->render('pages/index.html.twig');
    }
    
   /**
     * @Route("/layout", name="layout")
     */
    public function layout(): Response
    {
        
        return $this->render('Partials/layout.html.twig');
    }






 /**
     * @Route("/addCommande", name="add_commande")
    
     */

    public function addCommande(Request $request): Response
    {
        $commande= new Commandes();
        $valider=$request->query->get("valider");
        $reference=$request->query->get("reference");
        $em= $this->getDoctrine()->getManager();
        $date=new \DateTime('now');
        $commande->setValider(1);
        $commande->setReference(1);
        $commande->setDate($date);
       $em->persist($commande);
            $em->flush();
            $serializer=new Serializer([new ObjectNormalizer()]);
            $formatted=$serializer->normalize($commande);
            return new JsonResponse($formatted);
        }
    
/**
     * @Route("/deleteCommande", name="delete_commande")
    
     */
    public function deleteCommande(Request $request): Response
    {
        $id=$request->get("id");
        $em= $this->getDoctrine()->getManager();
        $commande=$em->getRepository(Commandes::class)->find($id);
        if($commande!=null){
        $em->remove($commande);
        $em->flush();
        $serialize=new Serializer([new ObjectNormalizer()]);
        $formatted=$serialize->normalize("Commande a ete supprimer avec succés");
        return new JsonResponse($formatted);
        }
        return new JsonResponse("id commande invalide");
    }
/**
     * @Route("/updateCommande", name="update_commande")
     
     */
    public function modifierCommandeAction(Request $request): Response
    {
        $em= $this->getDoctrine()->getManager();
        $commande=$this->getDoctrine()->getManager()->getRepository(Commandes::class)->find($request->get("id"));
   
        $commande->setReference($request->get("reference"));
        $commande->setValider($request->get("valider"));
       
       $em->persist($commande);
            $em->flush();
            $serializer=new Serializer([new ObjectNormalizer()]);
            $formatted=$serializer->normalize($commande);
            return new JsonResponse("Commande a ete modifier avec succees");
    }
        /**
     * @Route("/displayCommande", name="display_commande")
     */

     public function allCommandeAction(){
        $em= $this->getDoctrine()->getManager();
        $commande=$em->getRepository(Commandes::class)->findAll();
        $serializer=new Serializer([new ObjectNormalizer()]);
        $formatted=$serializer->normalize($commande);
        return new JsonResponse($formatted);
    }
    
    
    
}
