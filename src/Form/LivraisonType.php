<?php

namespace App\Form;

use App\Entity\Commande;
use App\Entity\Livraison;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LivraisonType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
       $builder
            ->add('nom')
            ->add('prix')
            ->add('typelivraison', ChoiceType::class, array(
                'choices' => array(
                    '------------ Sélectionnez le type de la livraison --------------' => null,
                    'Decoration' => 'Decoration',
                    'vetements' => 'vetements',
                    'accessoires' => 'accessoires',
                )))
           ->add('commandes', EntityType::class, [
               'class' => Commande::class,
               'query_builder' => function (EntityRepository $er) {
                   return $er->createQueryBuilder('c')
                       ->andWhere('c.livraison IS NULL');
               },
               'choice_label' => 'libelle',
               'multiple' => true,
           ])
           ->add('ajouter',SubmitType::class, ['label'=>'Mettre à jour'])
            ;

    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Livraison::class,
        ]);
    }
}
