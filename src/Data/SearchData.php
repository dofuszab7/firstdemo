<?php

namespace App\Data;
class SearchData
{

    /**
     * @var string
     */

     public $q = '';

      /**
     * @var Categorie[]
     */

    public $categories = [];
     /**
     * @var null|string
     */
    public $max;
    /**
     * @var null|string
     */
    public $min;



}